package ExcelService.Models;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.JsonNode;

import java.util.List;

public class ExcelData {

    @JsonProperty
    private List<String> headings;

    @JsonProperty
    private List<JsonNode> data;

    public List<String> headings() {
        return headings;
    }

    public List<JsonNode> data() {
        return data;
    }
}
