package ExcelService.Controllers;

import ExcelService.Models.ExcelData;
import ExcelService.Services.ExcelService;
import com.fasterxml.jackson.databind.JsonNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.MediaType;
import org.springframework.util.ResourceUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;

@RestController
@RequestMapping("/api/excel")
@CrossOrigin
public class ExcelController {

    @Autowired
    private ExcelService excelService;

    private static int serialNumber = 0;
    private static final int MAX_SERIAL = 15;
    private static final String BASE_EXCEL_NAME = "ExportedExcel";
    private static final String BASE_EXCEL_POSTFIX = ".xls";

    @PostMapping("/headings")
    @ResponseBody
    public int createExcel(@RequestBody ExcelData excelData) throws Exception {
        int currSerialNumber = getNextSerialNumber();
        String excelFileName = BASE_EXCEL_NAME + currSerialNumber + BASE_EXCEL_POSTFIX;
        this.excelService.createExcel(excelData.headings(), excelData.data(), excelFileName);
        return currSerialNumber;
    }

    @PostMapping("")
    public int createExcel(@RequestBody List<JsonNode> nodeList) throws Exception {
        int currSerialNumber = getNextSerialNumber();
        String excelFileName = BASE_EXCEL_NAME + currSerialNumber + BASE_EXCEL_POSTFIX;
        this.excelService.createExcel(this.excelService.getHeadings(nodeList), nodeList, excelFileName);
        return currSerialNumber;
    }

    @RequestMapping(produces = MediaType.APPLICATION_OCTET_STREAM_VALUE, method = RequestMethod.GET)
    @ResponseBody
    public FileSystemResource downloadExcel(@RequestParam int excelId, @RequestParam(required = false, defaultValue = "ExportedExcel") String excelName, HttpServletResponse response) throws FileNotFoundException {
        String fileName = BASE_EXCEL_NAME + excelId + BASE_EXCEL_POSTFIX;
        response.setContentType("application/xls");
        response.setHeader("Content-disposition", "attachment; filename="+ excelName + BASE_EXCEL_POSTFIX);
        FileSystemResource fileSystemResource = new FileSystemResource(ResourceUtils.getFile(fileName));
        return fileSystemResource;
    }

    private synchronized int getNextSerialNumber() {
        serialNumber++;

        if (serialNumber == MAX_SERIAL) {
            serialNumber = 1;
        }

        return serialNumber;
    }
}
