package ExcelService.Services;

import com.fasterxml.jackson.databind.JsonNode;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Service
public class ExcelService {

    public void createExcel(List<String> headings, List<JsonNode> nodeList, String excelName) throws Exception {
        Workbook workbook = new HSSFWorkbook();
        Sheet sheet = workbook.createSheet("ExportedExcel");
        sheet.setRightToLeft(true);

        // Headings
        CellStyle headingsStyle = this.getHeadingsStyle(sheet);
        Row headingsRow = sheet.createRow(1);
        int headingIndex = 1;

        for (String currHerading : headings) {
            Cell headingCell = headingsRow.createCell(headingIndex);
            headingCell.setCellValue(currHerading);
            headingCell.setCellStyle(headingsStyle);
            headingIndex++;
        }

        // Data
        CellStyle cellStyle = this.getCellsStyle(sheet);
        int rowIndex = 2;
        for (JsonNode node : nodeList) {
            Row newRow = sheet.createRow(rowIndex);
            rowIndex++;

            Iterator<String> dataHeadings = node.fieldNames();
            int dataCellIndex = 1;

            while (dataHeadings.hasNext()) {
                String currHeading = dataHeadings.next();
                Cell newCell = newRow.createCell(dataCellIndex);
                newCell.setCellValue(node.findPath(currHeading).asText());
                newCell.setCellStyle(cellStyle);
                dataCellIndex++;
            }
        }

        // Try to create the excel
        try (FileOutputStream out = new FileOutputStream(excelName)) {
            ((HSSFWorkbook) workbook).write(out);
        }
    }

    public List<String> getHeadings(List<JsonNode> nodeList) {
        List<String> headings = new ArrayList<String>();

        Iterator<String> headingsIterator = nodeList.get(0).fieldNames();
        while (headingsIterator.hasNext()) {
            headings.add(headingsIterator.next());
        }

        return headings;
    }

    public CellStyle getCellsStyle(Sheet sheet) {
        final Font font = sheet.getWorkbook ().createFont ();
        font.setFontName ( "Heebo" );
        font.setColor (IndexedColors.BLACK.index);

        final CellStyle style = sheet.getWorkbook().createCellStyle();
        style.setFont(font);
        style.setBorderBottom(BorderStyle.THIN);
        style.setBorderTop(BorderStyle.THIN);
        style.setBorderRight(BorderStyle.THIN);
        style.setBorderLeft(BorderStyle.THIN);
        style.setBottomBorderColor(IndexedColors.BLACK.index);
        style.setTopBorderColor(IndexedColors.BLACK.index);
        style.setLeftBorderColor(IndexedColors.BLACK.index);
        style.setRightBorderColor(IndexedColors.BLACK.index);

        return style;
    }

    public CellStyle getHeadingsStyle(Sheet sheet) {
        final Font font = sheet.getWorkbook ().createFont ();
        font.setFontName ( "Heebo" );
        font.setColor (IndexedColors.WHITE.index);

        final CellStyle style = sheet.getWorkbook().createCellStyle();
        style.setFont (font);
        style.setFillForegroundColor( IndexedColors.BLACK.index );
        style.setFillPattern ( FillPatternType.SOLID_FOREGROUND );
        style.setBorderBottom(BorderStyle.THICK);
        style.setBorderTop(BorderStyle.THICK);
        style.setBorderRight(BorderStyle.THICK);
        style.setBorderLeft(BorderStyle.THICK);
        style.setBottomBorderColor(IndexedColors.BLACK.index);
        style.setTopBorderColor(IndexedColors.BLACK.index);
        style.setLeftBorderColor(IndexedColors.BLACK.index);
        style.setRightBorderColor(IndexedColors.BLACK.index);

        return style;
    }


}
